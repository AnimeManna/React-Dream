import React from 'react'
import Home from '../home/home.js'
import Sidebar from '../sidebar/sidebar'

class Page extends React.Component{


    constructor(props){
        super(props);
        this.state = {
            general:{
            },
            job:{
            },
            address:{
            },
            contact:{
            },
            loading:false
        }

    }

    changeState(newInfo){
        this.setState({
            loading:true,
            ...newInfo
        })
    }


    render(){
        return(
            <div className="page">
                {this.props.sidebarStatus
                    ? <Sidebar
                        changeParentStates={(newInfo)=>{
                    this.changeState(newInfo)
                }} data={this.props.data} />
                    : null}
                <Home
                    {...this.state}
                />

            </div>
        )
    }
}

export default Page