import React from 'react';


class Home extends React.Component {
    constructor(props) {
        super(props)
    }

    render(){
        const {firstName, lastName, avatar} = this.props.general
        const {company,title} = this.props.job
        const {email,phone}=this.props.contact
        const {street,city,zipCode,country}=this.props.address
        console.log(this.props)
        return(
            <div className="home">{this.props.loading
            ?
                <div className="home__item">
                    <img
                        src={avatar}
                        className="home__img"
                    />
                    <div className="home__content">
                        <div className="home__title">{firstName} {lastName}</div>
                        <div className="home__address">{title} {company}</div>
                        <div className="home__info">{phone}</div>
                        <div className="home__info">{email}</div>
                        <div className="home__info">{street},{city},{zipCode},{country}</div>
                    </div>

                </div>
            :null}

            </div>
        )
    }
}

export default Home