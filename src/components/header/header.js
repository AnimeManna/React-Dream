import React from 'react'
import FaDehaze from 'react-icons/lib/md/dehaze';

class Header extends React.Component{;

    render(){

        return(
            <div className="header">
                    <FaDehaze className="
                    headline-burger
                    "
                              onClick = {()=> {
                        let newState = !this.props.status;
                        this.props.changeParentState(newState);
                    }}  />
                    <div className="
                    headline-name
                    ">
                </div>
            </div>
        )
    }
}

export default Header