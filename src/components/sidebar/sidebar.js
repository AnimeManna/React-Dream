import React from 'react';
import {Link} from 'react-router-dom'


class Sidebar extends React.Component{
    render(){
        return(
            <div className="sidebar">
                {this.props.data.map((value,index)=>{
                    return (
                        <Link to='/client'>
                            <div
                                key={index}
                                className="sidebar__item"
                                onClick={()=>{
                                    this.props.changeParentStates(value);
                                }}>
                                <img
                                    className="sidebar_img"
                                    src={value.general.avatar}/>
                                <div className="sidebar__content">
                                    {value.general.firstName} {value.general.lastName}</div>
                            </div>
                        </Link>)
                })}

            </div>
        )
    }
}

export default Sidebar