import './styles/main.css'

import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app/app.js'
import Sidebar from './components/sidebar/sidebar'
import { BrowserRouter, Link,Route } from 'react-router-dom';


ReactDOM.render(
    <BrowserRouter>
        <div>
            <Route path='/' component={App}>
                <Route path='sidebar' component={Sidebar}/>
            </Route>
        </div>
    </BrowserRouter>
    ,document.getElementById('root')
);

module.hot.accept();